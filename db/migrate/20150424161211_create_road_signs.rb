class CreateRoadSigns < ActiveRecord::Migration
  def change
    create_table :road_signs do |t|

      t.string :name
      t.string :image

      t.timestamps
    end
  end
end
