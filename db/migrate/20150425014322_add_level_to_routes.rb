class AddLevelToRoutes < ActiveRecord::Migration
  def change
    add_column :routes, :level, :integer
  end
end
