class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.integer :rails_id
      t.integer :wayspoint_id

      t.timestamps
    end
  end
end
