class CreateWaypointTable < ActiveRecord::Migration
  def change
    create_table :waypoint_tables do |t|
    	t.string :name
    	t.float :lat
    	t.float :lag    	
    end
  end
end
