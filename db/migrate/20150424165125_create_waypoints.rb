class CreateWaypoints < ActiveRecord::Migration
  def change
    create_table :waypoints do |t|
	  t.string :name
	  t.float :lat
      t.float :lag  
      t.timestamps
    end
  end
end
