# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

our = Waypoint.create(name: "OUR", lat: 14.653729422751928, lag: 121.06515124434736);
faculty = Waypoint.create(name: "Faculty", lat: 14.6537756, lag: 121.0684634);
palma = Waypoint.create(name: "Palma Hall", lat: 14.653843557859798, lag: 121.07072867453098);
vinzons = Waypoint.create(name: "Vinzons Hall", lat: 14.65434373616606, lag: 121.07297569513321);
econ = Waypoint.create(name: "Economics Building", lat: 14.6558369, lag: 121.072954);
katipexit = Waypoint.create(name: "Katipunan Exit", lat: 14.657385003285356, lag: 121.07275307178497);
is = Waypoint.create(name: "International Center", lat: 14.658389234947467, lag: 121.07275575399399);
cri = Waypoint.create(name: "CRI", lat: 14.659379190077114, lag: 121.07169091701508);
sc = Waypoint.create(name: "Shopping Center 1", lat: 14.659377892629015, lag: 121.07028007507324);
sc2 = Waypoint.create(name: "Shopping Center 2", lat: 14.659333779389263, lag: 121.06885313987732);
kalay = Waypoint.create(name: "Kalay", lat: 14.65883565597751, lag: 121.06855664400882);
mr = Waypoint.create(name: "Magsaysay-Roces", lat: 14.6575122516441, lag: 121.06855798511333);
engg = Waypoint.create(name: "Engineering", lat: 14.656247322489643, lag: 121.06857530769958);
music = Waypoint.create(name: "Music", lat: 14.65604121969026, lag: 121.0660739244122);

dcs = Waypoint.create(name: "DCS", lat: 14.648655865170278, lag: 121.06898099181308);
eee = Waypoint.create(name: "EEE", lat: 14.649792482374064, lag: 121.06862694022311);
aspav = Waypoint.create(name: "AS Pavilion", lat: 14.652403046524151, lag: 121.06877177950992);
che = Waypoint.create(name: "CHE", lat: 14.652714443759868, lag: 121.07165783632809);
ben = Waypoint.create(name: "Benitez Hall", lat: 14.653659012667902, lag: 121.07165783632809);
