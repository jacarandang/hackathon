Rails.application.routes.draw do
  resources :waypoints
  resources :routes do
  	match '/add_waypoints', :to => 'routes#add_waypoints', :via => :get, :as => :add_waypoints
  	match '/add_link/:waypoint_id', :to => 'routes#add_link', :via => :get, :as => :add_link
  end


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  devise_for :users
  # root 'static_pages#home'
  # You can have the root of your site routed with "root"
  root 'welcome#index'
  get '/game/:level', :to => 'welcome#game', as: :welcome_game
  get '/levelSelect', :to => 'welcome#levelSelect', as: :welcome_select
  match "/guess_the_road_sign", :to => "guess_road_sign#index", :via => :get, :as => :grs_index
  match "/guess_the_road_sign/start", :to => "guess_road_sign#start", :via => :get, :as => :grs_start
  match "/guess_the_road_sign/:page", :to => "guess_road_sign#page", :via => :get, :as => :grs_page
  post "/guess_the_road_sign/:page/check", :to => "guess_road_sign#check", as: :grs_check
  #match "/search/companies", :to => "search#show_companies_search", :via => :get, :as => :search_companies

  resources :road_sign
  resources :levels do
    match '/set_start', :to => "levels#set_start", :via => :get, :as => :set_start
    match '/save_start/:waypoint_id', :to => "levels#save_start", :via => :get, :as => :save_start
    match '/set_end', :to => "levels#set_end", :via => :get, :as => :set_end
    match '/save_end/:waypoints_id', :to => "levels#save_end", :via => :get, :as => :save_end
  end
end
