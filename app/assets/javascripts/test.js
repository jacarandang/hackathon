// function Waypoint(name, lnglat, nxt){
//   this.name = name
//   this.latlng = lnglat;
//   this.nxt = nxt;
//   this.lat = this.latlng.lat;
//   this.lng = this.latlng.lng;
// }
//
// function Route(){
//   this.route = []
//   this.fare = 7.50;
// }
//
// our = new Waypoint("OUR", new google.maps.LatLng(14.653729422751928, 121.06515124434736), null);
// faculty = new Waypoint("Faculty", new google.maps.LatLng(14.6537756, 121.0684634), null);
// palma = new Waypoint("Palma Hall", new google.maps.LatLng(14.653843557859798, 121.07072867453098), null);
// vinzons = new Waypoint("vinzons Hall", new google.maps.LatLng(14.65434373616606, 121.07297569513321), null);
// econ = new Waypoint("Economics Building", new google.maps.LatLng(14.6558369,121.072954), null);
// katipexit = new Waypoint("Katipunan Exit", new google.maps.LatLng(14.657385003285356, 121.07275307178497), null);
// is = new Waypoint("International Center", new google.maps.LatLng(14.658389234947467, 121.07275575399399), null);
// cri = new Waypoint("CRI", new google.maps.LatLng(14.659379190077114, 121.07169091701508), null);
// sc = new Waypoint("Shopping Center 1", new google.maps.LatLng(14.659377892629015, 121.07028007507324), null);
// sc2 = new Waypoint("Shopping Center 2", new google.maps.LatLng(14.659333779389263, 121.06885313987732), null);
// kalay = new Waypoint("Kalay", new google.maps.LatLng(14.65883565597751, 121.06855664400882), null);
// mr = new Waypoint("Magsaysay-Roces", new google.maps.LatLng(14.6575122516441, 121.06855798511333), null);
// engg = new Waypoint("Engineering", new google.maps.LatLng(14.656247322489643, 121.06857530769958), null);
// music = new Waypoint("Music", new google.maps.LatLng(14.65604121969026, 121.0660739244122), null);
//
// dcs = new Waypoint("DCS", new google.maps.LatLng(14.648655865170278, 121.06898099181308), null);
// eee = new Waypoint("EEE", new google.maps.LatLng(14.649792482374064, 121.06862694022311), null);
// aspav = new Waypoint("AS Pavilion", new google.maps.LatLng(14.652403046524151, 121.06877177950992), null);
// che = new Waypoint("CHE", new google.maps.LatLng(14.652714443759868, 121.07165783632809), null);
// ben = new Waypoint("Benitez Hall", new google.maps.LatLng(14.653659012667902, 121.07165783632809), null);
//
// philcoa = new Route(faculty, music);
// philcoa.route.push(our, faculty, palma, vinzons, econ, katipexit, is, cri, sc, sc2, kalay, mr, engg, music);
// ikot = new Route(dcs, mr);
// ikot.route.push(dcs, eee, aspav, che, ben, vinzons, econ, katipexit, is, cri, sc, sc2, kalay, mr);
//
// var dirDisplay;
// var dirService = new google.maps.DirectionsService();
// var map;
//
// var currentPosition = dcs;
// var currentMarker = null;
// var currentRoute = null;
// var target = kalay;
//
//
// var routeMarkers = []
// var positions = []
// var fund = 7.50
//
// function initialize() {
//
//   dirDisplay = new google.maps.DirectionsRenderer();
//
//   var position = currentPosition.latlng; //{ lat: 14.6549302, lng: 121.0642827};
//   var mapOptions = {
//     center: position,
//     zoom: 17
//   };
//   map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
//   dirDisplay.setMap(map);
//   google.maps.event.addListener(map, 'click', function(e){ console.log(e)});
//   setCurrentMarker();
//   updateFunds(fund);
//
//   var transitLayer = new google.maps.TrafficLayer();
//   transitLayer.setMap(map);
//
// }
//
// function setCurrentMarker(){
//   if(currentMarker != null){
//     currentMarker.setMap(null);
//   }
//   var infowindow = new google.maps.InfoWindow({
//     content: "You're here!"
//   });
//
//   var pos = currentPosition;
//   var marker = new google.maps.Marker({
//     position: pos.latlng,
//     icon: {
//       path: google.maps.SymbolPath.CIRCLE,
//       scale: 10
//     },
//     map: map,
//     title: "Player",
//   });
//   google.maps.event.addListener(marker, "click", function(){infowindow.open(map, marker)});
//   currentMarker = marker;
//   setText(currentPosition.name);
// }
//
// function clearRoute(route){
//   for(var i = 0; i < routeMarkers.length; i++){
//     routeMarkers[i].setMap(null);
//   }
//   routeMarkers = [];
//   positions = [];
//   currentRoute = null;
// }
//
// function viewRoute(route){
//   clearRoute();
//   for(var i = 0; i < route.route.length; i++){
//     current = route.route[i];
//     if(current == currentPosition) continue;
//     var mark = new google.maps.Marker({
//       position: current.latlng,
//       map: map,
//       animation: google.maps.Animation.DROP,
//       title: current.name
//     });
//
//     routeMarkers.push(mark);
//     positions.push(current);
//     google.maps.event.addListener(mark, 'click', changePosition);
//     currentRoute = route;
//   }
// }
//
// function changePosition(e){
//   if(fund < currentRoute.fare){
//     $('#errors').text("No fare left!");
//     return;
//   }
//   else{
//     fund -= currentRoute.fare;
//     updateFunds(fund);
//   }
//   var idx;
//   for(var i = 0; i < routeMarkers.length; i++){
//     if(routeMarkers[i].position == e.latLng){
//       idx = i;
//       break;
//     }
//   }
//   currentPosition = positions[idx];
//   clearRoute();
//   setCurrentMarker();
// }
//
// function calcRoute() {
//   var start = philcoa.start.latlng;
//   var end = philcoa.end.latlng;
//   var waypoints = [];
//
//   var cur = philcoa.start;
//   while(cur.nxt != null){
//     cur = cur.nxt
//     waypoints.push({location: cur.latlng, stopover: true});
//   }
//   var request = {
//     origin: start,
//     destination: end,
//     waypoints: waypoints,
//     optimizeWaypoints: true,
//     travelMode: google.maps.TravelMode.DRIVING
//   };
//
//   dirService.route(request, function(response, status){
//     if(status == google.maps.DirectionsStatus.OK){
//       dirDisplay.setDirections(response);
//       console.log(response);
//       for(var i = 0; i < response.routes[0].overview_path.length; i++){
//         new google.maps.Marker({
//           position: response.routes[0].overview_path[i],
//           map: map,
//           title: "Player"
//         });
//       }
//     }
//   });
// }
//
// function setText(text){
//   $("#currentPos").text(currentPosition.name);
// }
//
// function updateFunds(text){
//   $("#currentFun").text(text);
// }
//
// google.maps.event.addDomListener(window, 'load', initialize);
