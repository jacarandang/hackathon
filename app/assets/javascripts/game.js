function Waypoint(name, lnglat, nxt){
  this.name = name
  this.latlng = lnglat;
  this.nxt = nxt;
  this.lat = this.latlng.lat;
  this.lng = this.latlng.lng;
}

function Route(){
  this.route = []
  this.fare = 7.50;
}

var dirDisplay;
var dirService = new google.maps.DirectionsService();
var map;

var currentPosition = null;
var currentMarker = null;
var currentRoute = null;
var target = null;


var routeMarkers = []
var positions = []
var fund = 15.0

function initialize() {

  dirDisplay = new google.maps.DirectionsRenderer();

  var position = currentPosition.latlng; //{ lat: 14.6549302, lng: 121.0642827};
  var mapOptions = {
    center: position,
    zoom: 17
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  dirDisplay.setMap(map);
  google.maps.event.addListener(map, 'click', function(e){ console.log(e)});
  setCurrentMarker();
  updateFunds(fund);
  $('#currentTarget').text(target.name);

  var transitLayer = new google.maps.TrafficLayer();
  transitLayer.setMap(map);

}

function setCurrentMarker(){
  if(currentMarker != null){
    currentMarker.setMap(null);
  }
  var infowindow = new google.maps.InfoWindow({
    content: "You're here!"
  });

  var pos = currentPosition;
  var marker = new google.maps.Marker({
    position: pos.latlng,
    icon: {
      path: google.maps.SymbolPath.CIRCLE,
      scale: 10
    },
    map: map,
    title: "Player",
  });
  google.maps.event.addListener(marker, "click", function(){infowindow.open(map, marker)});
  currentMarker = marker;
  setText(currentPosition.name);
}

function clearRoute(route){
  for(var i = 0; i < routeMarkers.length; i++){
    routeMarkers[i].setMap(null);
  }
  routeMarkers = [];
  positions = [];
  currentRoute = null;
}

function viewRoute(route){
  var found = false;
  for(var i = 0; i < route.route.length; i++){
    console.log(route.route[i]);
    console.log(currentPosition);
    if(route.route[i].name == currentPosition.name){
      found = true;
      break;
    }
  }
  if(!found){
    $('#errors').text("You can't use this route now!");
    return;
  }
  clearRoute();
  for(var i = 0; i < route.route.length; i++){
    current = route.route[i];
    if(current.name == currentPosition.name) continue;
    var mark = new google.maps.Marker({
      position: current.latlng,
      map: map,
      animation: google.maps.Animation.DROP,
      title: current.name
    });

    routeMarkers.push(mark);
    positions.push(current);
    google.maps.event.addListener(mark, 'click', changePosition);
    currentRoute = route;
  }
}

function changePosition(e){
  if(fund < currentRoute.fare){
    $('#errors').text("Not enough funds!");
    return;
  }
  else{
    fund -= currentRoute.fare;
    updateFunds(fund);
  }
  var idx;
  for(var i = 0; i < routeMarkers.length; i++){
    if(routeMarkers[i].position == e.latLng){
      idx = i;
      break;
    }
  }
  currentPosition = positions[idx];
  clearRoute();
  setCurrentMarker();

  if(currentPosition.name == target.name){
    $('#errors').text("You've reached your destination!");
    return
  }
}

function calcRoute() {
  var start = philcoa.start.latlng;
  var end = philcoa.end.latlng;
  var waypoints = [];

  var cur = philcoa.start;
  while(cur.nxt != null){
    cur = cur.nxt
    waypoints.push({location: cur.latlng, stopover: true});
  }
  var request = {
    origin: start,
    destination: end,
    waypoints: waypoints,
    optimizeWaypoints: true,
    travelMode: google.maps.TravelMode.DRIVING
  };

  dirService.route(request, function(response, status){
    if(status == google.maps.DirectionsStatus.OK){
      dirDisplay.setDirections(response);
      console.log(response);
      for(var i = 0; i < response.routes[0].overview_path.length; i++){
        new google.maps.Marker({
          position: response.routes[0].overview_path[i],
          map: map,
          title: "Player"
        });
      }
    }
  });
}

function setText(text){
  $("#currentPos").text(currentPosition.name);
}

function updateFunds(text){
  $("#currentFun").text(text);
}
