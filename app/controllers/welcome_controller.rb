class WelcomeController < ApplicationController

  def index
  end

  def levelSelect
    @levels = Level.all
  end

  def game
    @level = Level.find(params[:level])
    @start = Waypoint.find(@level.start)
    @end = Waypoint.find(@level.endd)
    @routes = Route.all
    @waypoints = []
    @routes.each do |r|
      @links = Link.where(rails_id: r.id )
      @wp = []

      @links.each do |l|
        @wp << Waypoint.find(l.wayspoint_id).id
      end

      @waypoints << Waypoint.where(id: @wp)
    end

  end
end
