class RoutesController < ApplicationController
  before_action :set_route, only: [:show, :edit, :update, :destroy]

  # GET /routes
  # GET /routes.json
  def index
    @routes = Route.all
  end

  # GET /routes/1
  # GET /routes/1.json
  def show
  	@route = Route.find(params[:id])
  end
  
  def add_waypoints
  	@waypoints_list = Waypoint.all
  	@route = Route.find(params[:route_id])
  	@links = Link.where(rails_id: @route.id)
  	@wp = []
  	@idlist = []

  	@links.each do |l|
  		@wp << Waypoint.find(l.wayspoint_id).id
  	end
  	
  	@waypoints_list = Waypoint.where.not(id: @wp).all
    @waypoints_list2 = Waypoint.where(id: @wp).all
  end
  
  def add_link
  	@route = Route.find(params[:route_id])
  	@waypoint = Waypoint.find(params[:waypoint_id])
  	@link = Link.new
  	@link.update(rails_id: @route.id)
  	@link.update(wayspoint_id: @waypoint.id)
    redirect_to route_add_waypoints_path(@route.id)
  end

  # GET /routes/new
  def new
    @route = Route.new
  end

  # GET /routes/1/edit
  def edit
  end

  # POST /routes
  # POST /routes.json
  def create
    @route = Route.new(route_params)

    #respond_to do |format|
      if @route.save
        #format.html { redirect_to @route, notice: 'Route was successfully created.' }
        #format.json { render :show, status: :created, location: @route }
        redirect_to routes_path
      else
        format.html { render :new }
        format.json { render json: @route.errors, status: :unprocessable_entity }
      end
  end

  # PATCH/PUT /routes/1
  # PATCH/PUT /routes/1.json
  def update
    respond_to do |format|
      if @route.update(route_params)
        format.html { redirect_to @route, notice: 'Route was successfully updated.' }
        format.json { render :show, status: :ok, location: @route }
      else
        format.html { render :edit }
        format.json { render json: @route.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /routes/1
  # DELETE /routes/1.json
  def destroy
    @route.destroy
    respond_to do |format|
      format.html { redirect_to routes_url, notice: 'Route was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_route
      @route = Route.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def route_params
      params.require(:route).permit(:name)
    end
end
