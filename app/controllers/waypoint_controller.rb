class WaypointController < ApplicationController

	def create
	end
	
	def update
	end
	
	def store
		@waypoint = Waypoint.new waypoint_param
		if @waypoint.save
			redirect waypoint_index_path
		else
			render 'new'
		end
	end
	
	def index
		@waypoints = Waypoint.all
	end
	
	def new
		@waypoint = Waypoint.new
	end
	
	private
	
		def waypoint_param
			params.require(:waypoint).permit(:name, :lat, :lag)
		end
end
