class GuessRoadSignController < ApplicationController

  def index
    @road_signs = RoadSign.all
  end

  def start
    @road_signs = RoadSign.all
    @answers = []

    @rand_list = []
    for i in 1..5
      @a = rand(@road_signs.length) + 1
      while @rand_list.include? @a do
        @a = rand(@road_signs.length) + 1
      end
      @rand_list << @a
      @sign = RoadSign.find @a
      @answers << @sign.name
    end

    session[:questions] = @rand_list
    session[:answers] = @answers
    session[:points] = 0
    redirect_to grs_page_path(1)
  end

  def page
    @index = params[:page].to_i - 1
    @questions = session[:questions]
    @question = @questions[@index]
    @road_sign = RoadSign.find(@question)

  end

  def check
    @index = params[:page].to_i - 1

    if params[:answer][:answer] == session[:answers][@index]
      session[:points] += 1
    end

    if params[:page].to_i + 1 >= 5
      redirect_to grs_index_path
    else
      redirect_to grs_page_path(params[:page].to_i + 1)
    end
  end

end
