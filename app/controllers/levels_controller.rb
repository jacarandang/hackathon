class LevelsController < ApplicationController
  def new
    @level = Level.new
  end

  def create
    @level  = Level.create(level_params)
    if @level.save
      redirect_to levels_path
    else
      render 'new'
    end
  end

  def show
    @level = Level.find(params[:id])
  end

  def set_start
    @level = Level.find(params[:level_id])
    @waypoints = Waypoint.all
  end

  def save_start
    @level = Level.find(params[:level_id])
    @level.update(start: params[:waypoint_id])
    redirect_to level_path(@level.id)
  end

  def set_end
    @level = Level.find(params[:level_id])
    @waypoints = Waypoint.all
  end

  def save_end
    @level = Level.find(params[:level_id])
    @level.update(endd: params[:waypoints_id])
    redirect_to level_path(@level.id)
  end

  def index
    @levels = Level.all
  end

  private
    def level_params
      params.require(:level).permit(:fare)
    end
end
