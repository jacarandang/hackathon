class RoadSignController < ApplicationController

  def new
    @road_sign = RoadSign.new
  end

  def create
    @road_sign = RoadSign.create(road_sign_params)
    if @road_sign.save
      redirect_to grs_index_path
    else
      render 'new'
    end
  end

  private
    def road_sign_params
      params.require(:road_sign).permit(:name, :image)
    end
end
