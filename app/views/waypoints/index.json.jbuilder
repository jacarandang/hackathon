json.array!(@waypoints) do |waypoint|
  json.extract! waypoint, :id
  json.url waypoint_url(waypoint, format: :json)
end
